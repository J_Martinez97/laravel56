
@extends('layouts.app')
@section('content')

<div>
    <h1>Create Reserve</h1>
    <form action="/reserves"  method="post">
        {{ csrf_field() }}
        <label>
            User Name:
            <select name="user_name">
                @foreach($users as $user)
                <option value="{{$user->id}}"> {{$user->name}}</option>
               @endforeach
            </select>
        </label><br>

        <label>Resource:
            <select name="resource">
                @foreach($resources as $resource)
                <option value="{{$resource->id}}">{{$resource->name}}</option>
               @endforeach
            </select>
        </label>
        <label>Date In:</label>
            <input type="text" name="date_in">
            <br>
        <label>Date limit:</label>
            <input type="text" name="date_limit">

        <input type="submit" name="save" value="Save">
        <a href="/reserves">
            <input type="button" name="return" value="Return">
        </a>
    </form>
</div>
@endsection
