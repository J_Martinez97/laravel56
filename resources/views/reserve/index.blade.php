@extends('layouts.app')
@section('content')
<div class="container">
<h1>List</h1>
<table class="table table-bordered">
    <tr>
        <th>User Name</th>
        <th>Recursos</th>
        <th>Date In</th>
        <th>Date Limit</th>
        <th></th>
    </tr>
    @foreach($reserves as $reserve)
    <tr>
        <td>{{ $reserve->user->name }}</td>
        <td>{{ $reserve->resource->name }}</td>
        <td>{{ $reserve->date_in }}</td>
        <td>{{ $reserve->date_limit }}</td>
        <td >
            <a href="/reserves/{{ $reserve->id }}/edit"><input type="button" value="Edit"></a>--
            <form method="post" action="/reserves/{{ $reserve->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="delete">

                <input type="submit" value="Delete">
            </form>
        </td>
    </tr>
    @endforeach
</table>
<a href="/reserves/create">
        <input type="button" name="new" value="New">
    </a>
</div>
@endsection
