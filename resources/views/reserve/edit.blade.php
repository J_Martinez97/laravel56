@extends('layouts.app')
@section('content')

<h1>Editar Reserva</h1>
<div>
    <form method="post" action="/reserves/{{ $reserve->id }}">
        {{ csrf_field() }}

        <input type="hidden" name="_method" value="put">
        <label>User Name</label>
            <input type="text" name="user_name" value="{{ $reserve->user->name }}">
       <br>
        <label>Resouce:</label>
            <input type="text" name="resource" value="{{ $resource->name }}">
        
        <br>
        <label>Date In</label>
            
            <input type="text" name="date_in" value="{{ $reserve->date_in }}">
        <br>
        <label>Date limit</label>
            
            <input type="text" name="date_limit" value="{{ $reserve->date_limit }}"><br>

        <input type="submit" name="save" value="Save">
        <a href="/reserves">
            <input type="button" name="return" value="Return">
        </a>
    </form>
</div>
@endsection