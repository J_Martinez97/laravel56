@extends('layouts.app')
@section('content')
<div class="container">
<h1>Resource List</h1>
<table class="table table-bordered">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Description</th>
        <th></th>
    </tr>

    @foreach($resources as $resource)
    <tr>
        <td>{{ $resource->id }}</td>
        <td>{{ $resource->name }}</td>
        <td>{{ $resource->description }}</td>
        <td>
            <form method="post" action="/resources/{{ $resource->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="delete">

                <input type="submit" value="Delete">
            </form>
        </td>
    </tr>
    @endforeach
</table>

<a href="/resources/create">New</a>
</div>
@endsection
