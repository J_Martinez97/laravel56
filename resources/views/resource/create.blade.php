@extends('layouts.app')
@section('content')
<div class="container">
	<h1>New Resource</h1>
    <form action="/resources"  method="post">
        {{ csrf_field() }}
        <div class="form-control">
        <label class="">Name</label>
        <input type="text" name="name" >
        </div>
        <div class="form-control">
        <label class="">Description</label>
        <input type="text" name="description" >
        </div>
        <div class="form-group">
        <input class="form-control" type="submit" name="save" value="Save">
        </div>
        <a href="/resources">
            <input type="button" name="return" value="Return">
        </a>
    </form>
</div>

@endsection
