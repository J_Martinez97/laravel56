@extends('layouts.app')
@section('content')
    <div class="container">
        <h1>Detalle Usuario</h1>
        <table class="table table-bordered">
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Email</th>
                <th>Contrasenia</th>
                <th>Rol</th>
            </tr>
            <tr>
                <td>{{ $user->id }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->password }}</td>
                <td>{{ $user->role_id }}</td>
            </tr>
        </table>
    </div>
@endsection
