@extends('layouts.app')
@section('content')
<div class="container">
    <h1>Alta de Usuario</h1>
<form method="post" action="/users">
    <div class="form-group">
        <label>Nombre</label>
        <input class="form-control" type="text" name="name">
    </div>
    <div class="form-group">
        <label>Email</label>
        <input class="form-control" type="email" name="email">
    </div>
    <div class="form-group">
        <label>Contrasenia</label>
        <input class="form-control" type="text" name="password">
    </div>
    <div class="form-group">
        <label></label>
        <input class="form-control" name="" type="submit" value="Nuevo">
    </div>
</form>
</div>

@endsection
