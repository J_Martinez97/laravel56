@extends('layouts.app')
@section('content')

<div class="container">
    <h1>Lista de Usuarios</h1>
<table class="table table-bordered">
    <tr>
        <th>Id</th>
        <th>Id rol</th>
        <th>Nombre</th>
        <th>Email</th>
    </tr>
    @foreach ($users as $user)
    <tr>
        <td>{{ $user->id }}</td>
        <td>{{ $user->role->name }}</td>
        <td>{{ $user->name }}</td>
        <td>{{ $user->email }}</td>
            <form action="/users/{{ $user->id }}">
                <a class="btn btn-info" href="/users/{{ $user->id }}">Ver</a>

                <a href="/users/{{ $user->id }}/edit" class="btn btn-info">Editar</a>
                {{ csrf_field() }}
            </form>
        </td>
    </tr>
    @endforeach
</table>
<a href="/users/create">Nuevo</a>
</div>
@endsection

