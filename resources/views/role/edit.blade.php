@extends('layouts.app')
@section('content')
<div class="container">
<h1>Edición de Roles</h1>
<form method="post" action="/roles/{{ $role->id }}">
    {{ csrf_field() }}

    <input type="hidden" name="_method" value="put">
    <div class="form-control">
    <label>Nombre</label>
    <input type="text" name="name" class="form-control" value="{{ $role->role }}">
    </div>
     <div class="form-group">
    <input type="submit" name="" value="Editar" class="form-control">
    </div>
</form>
</div>
@endsection
