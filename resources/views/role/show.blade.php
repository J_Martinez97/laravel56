@extends('layouts.app')
@section('content')

<div class="container">
<h1>Detalle rol</h1>
<table class="table table-bordered">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Opciones</th>
    </tr>
    <tr>
        <td>{{ $role->id }}</td>
        <td>{{ $role->name }}</td>
        <td>
            <a href="/roles/{{ $role->id }}/edit">Editar</a>
            <form method="post" action="/roles/{{ $role->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="delete">
                <input type="submit" value="borrar">
            </form>
        </td>
    </tr>
</table>
<a href="/roles">Return</a>
</div>

@endsection
