@extends('layouts.app')
@section('content')
<div class="container">
    <h1>Lista de Roles</h1>
<table class="table table-bordered">
    <tr>
        <th>Id</th>
        <th>Nombre</th>
        <th>Opciones</th>
    </tr>
    @foreach ($roles as $role)
    <tr>
        <td>{{ $role->id }}</td>
        <td>{{ $role->name }}</td>
        <td>
            <a href="/roles/{{ $role->id }}">Ver</a> -
            <a href="/roles/{{ $role->id }}/edit">Editar</a>
            <form method="post" action="/roles/{{ $role->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="delete">
                <input type="submit" value="borrar">
            </form>
        </td>
    </tr>
    @endforeach
</table>
<a href="/roles/create">Nuevo</a>
</div>
@endsection

