<?php

namespace App\Policies;

use App\User;
use App\Reserve;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReservePolicy
{
    use HandlesAuthorization;

     public function index(User $user)
    {
        if(!$user->role_id)
        {
            return true;
        }else{
            return redirect("/login");;
        }
    }

       public function show(User $user, Reserve $reserve)
    {
       if($user->role_id == 1)
       {
            return true;
        }else{
            if($reserve->user->name == $user->name){
                return true;
            }
        }
    }
}
