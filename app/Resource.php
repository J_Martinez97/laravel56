<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
   protected $fillable = ['id', 'name', 'description'];


    public function reserve()
    {
       return $this->hasMany('App\Reserve');
    }
}
