<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SocialAuthController extends Controller
{
    public function redirect()
    {
        return Socialite::driver('google')->redirect();
    }

    public function callback()
    {
        dd('retorno');
        try {
            $user = Socialite::driver('google')->user();
        } catch (\Exception $e) {
            return redirect('/login')->with('status', 'Something went wrong or You have rejected the app!');
        }

        $authUser = $this->findOrCreateUser($user);
        Auth::login($authUser);
        return redirect('/home');
    }
    //public function callback()
//    {
//        dd('retorno');
//        try {
//            $user = Socialite::driver('google')->user();
//        } catch (\Exception $e) {
//            return redirect('/login')->with('status', 'Something went wrong or You have rejected the app!');
//        }//

//        $authUser = $this->findOrCreateUser($user);
//        Auth::login($authUser);
//        return redirect('/home');
//    }
}
