<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reserve extends Model
{

    protected $fillable = [
        'id', 'user_id', 'resource_id' , 'date_in','date_limit'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function resource()
    {
        return $this->belongsTo('App\Resource');
    }
}
