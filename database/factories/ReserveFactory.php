<?php

use Faker\Generator as Faker;
  
  
$factory->define(App\Reserve::class, function (Faker $faker) {
    return [
        'user_id' => rand(1, 3),
        'resource_id' => rand(1, 2), 
        'date_in' => $faker->dateTimeThisMonth()->format('Y-m-d'),
        'date_limit' => $faker->dateTimeThisMonth()->format('Y-m-d')
    ];
});
