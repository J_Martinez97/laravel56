<?php

use Illuminate\Database\Seeder;
use App\Reserve;

class ReserversTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

/*     factory(App\Reserve::class, 2)->create()->each(function($u) {
    $u->resource()->save(factory(App\Resource::class)->make());
  });
*/

           DB::table('reserves')->insert([
            'user_id' => 1 ,
            'resource_id' => 1 ,
            'date_in' => "2018-01-16",
            'date_limit' => "2018-02-16",
        ]);


           DB::table('reserves')->insert([
            'user_id' => 2 ,
            'resource_id' => 2 ,
            'date_in' => "2018-03-21",
            'date_limit' => "2018-04-21",
        ]);
    }
}
