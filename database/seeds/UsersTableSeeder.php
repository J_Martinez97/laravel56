<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
                'name' => 'Admin',
                'email' => 'elEmail@gmail.com',
                'password' => 'Admin',
                'role_id' => '1',

                ]);
        User::create([
                'name' => 'User2',
                'email' => 'elUser2@gmail.com',
                'password' => 'user2',
                'role_id' => '2',

                ]);
        User::create([
                'name' => 'User3',
                'email' => 'elUser3@gmail.com',
                'password' => 'user3',
                'role_id' => '3',

                ]);

    }
}
